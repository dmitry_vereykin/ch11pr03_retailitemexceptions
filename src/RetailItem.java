/**
 * Created by Dmitry on 7/13/2015.
 */
public class RetailItem {

    private String description;
    private int unitsOnHand;
    private double price;

    public RetailItem(String description, int unitsOnHand, double price) throws NegativeUnitsOnHand, NegativePrice {
        this.description = description;
        setUnitsOnHand(unitsOnHand);
        setPrice(price);
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setUnitsOnHand(int unit) throws NegativeUnitsOnHand {
        if (unit < 0)
            throw new NegativeUnitsOnHand();
        else
            this.unitsOnHand = unit;
    }

    public void setPrice(double price) throws NegativePrice {
        if (price < 0)
            throw new NegativePrice();
        else
            this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public int getUnitsOnHand() {
        return unitsOnHand;
    }

    public double getPrice() {
        return price;
    }

}
