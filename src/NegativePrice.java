/**
 * Created by Dmitry Vereykin on 7/24/2015.
 */
public class NegativePrice extends Exception {
    public NegativePrice() {
        super("Error! The price of item cannot be negative.");
    }
}
